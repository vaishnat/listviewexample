package com.example.lenovo.listviewexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ListView listviewanimals = (ListView) findViewById(R.id.listview1);
        final ArrayAdapter adapterAnimals = ArrayAdapter.createFromResource(this, R.array.AnimalsList, android.R.layout.simple_list_item_1);
        listviewanimals.setAdapter(adapterAnimals);
        listviewanimals.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick (AdapterView<?> parent ,View view,int position,long id){
                String message = getString(R.string.ToastMessage) + adapterAnimals.getItem(position);
                Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
            }

        });
    }
}
